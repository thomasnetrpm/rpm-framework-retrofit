

//Responsive Navigation
$(document).ready(function() {
	  $('body').addClass('js');
	  var $menu = $('#topnav'),
	  	  $menulink = $('.menu-link'),
	  	  $menuTrigger = $('.menu-item-has-children > a'),
	  	  $searchLink = $('.search-link'),
	  	  $siteSearch = $('.search-module'),
	  	  $siteWrap = $('.site-wrap');
	
	$searchLink.click(function(e) {
		e.preventDefault();
		$searchLink.toggleClass('active');
		$siteSearch.toggleClass('active');
	});

	$menulink.click(function(e) {
		e.preventDefault();
		$menulink.toggleClass('active');
		$menu.toggleClass('active');
		$siteWrap.toggleClass('nav-active');
	});
	
	$menuTrigger.click(function(e) {
		//e.preventDefault();
		var $this = $(this);
		$this.toggleClass('active').next('ul').toggleClass('active');
	});
	
});
